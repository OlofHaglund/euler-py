sum = 2

first = 1
second = 2

while(second < 4000000 and first < 4000000):
    if first < second:
        first = first + second
        if(first % 2 == 0):
            sum += first
    else:
        second = first + second
        if(second % 2 == 0):
            sum += second


print(sum)
