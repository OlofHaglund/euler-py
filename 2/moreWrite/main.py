sum = 2

first = 1
second = 2

while(second < 4000000):
    temp = first + second
    first = second
    second = temp
    if(temp % 2 == 0):
        sum += temp


print(sum)
