from math import sqrt; from itertools import count, islice

def is_prime(n):
    return n > 1 and all(n%i for i in islice(count(2), int(sqrt(n)-1)))

def find_div(n):
    for i in range(2, n):
        if n % i == 0:
            return i

    return n

number = 600851475143
largest_prime = 0

while(True):
    div = find_div(number)
    if div == number:
        break
    if is_prime(div) :
        if(div > largest_prime):
            largest_prime = div
        number = int(number / div)

print(div)
